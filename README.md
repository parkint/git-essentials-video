Git Essentials Video
====================

I was asked to create an instructional video for Learnable.  Working with the Content Director to get the right approach and target the appropriate audience took several iterations; work that is ideal for Git (and Github)!
